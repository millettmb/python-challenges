import datetime

now = datetime.datetime.now()

birthDate = raw_input("What date were you born? (DDMMYYYY)")
birthTime = raw_input("Do you know the time you were born? (HH:MM:SS) (if not, leave blank)")

if birthTime == '':
	birthTime = '00:00:00'

year = int(birthDate[4:])
month = int(birthDate[2:-4])
date = int(birthDate[:-6])
hour = int(birthTime[:-6])
minute = int(birthTime[3:-3])
second = int(birthTime[6:])

dateOfBirth = datetime.datetime(year, month, date, hour, minute, second)
print(now - dateOfBirth)
print((now - dateOfBirth).total_seconds())