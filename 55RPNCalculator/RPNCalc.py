#15 7 1 1 + - / 3 * 2 1 1 + + - should produce 5
#15 7 1 1 + - ^ 3 * 2 1 1 ^ + - should produce 2278122

equation = raw_input("Input equation: ")

if ',' in equation:
	delimiter = ','
else:
	delimiter = ' '

equation = equation.split(delimiter)

stack = []

for element in equation:
	reg1 = ''
	reg2 = ''
	if '+' in element:
		reg1 = stack.pop()
		reg2 = stack.pop()
		stack.append(int(reg1) + int(reg2))
		print(int(reg1) ,'+', int(reg2))
	elif '-' in element:
		reg1 = stack.pop()
		reg2 = stack.pop()
		stack.append(int(reg2) - int(reg1))
		print(int(reg2) ,'-', int(reg1))
	elif '*' in element:
		reg1 = stack.pop()
		reg2 = stack.pop()
		stack.append(int(reg1) * int(reg2))
		print(int(reg1) ,'*', int(reg2))
	elif '/' in element:
		reg1 = stack.pop()
		reg2 = stack.pop()
		stack.append(int(reg2) / int(reg1))
		print(int(reg2) ,'/', int(reg1))
	elif '^' in element:
		reg1 = stack.pop()
		reg2 = stack.pop()
		stack.append(int(reg2) ** int(reg1))
		print(int(reg2) ,'^', int (reg1))
	else:
		stack.append(int(element))
		print('popping ', element)

print(stack)