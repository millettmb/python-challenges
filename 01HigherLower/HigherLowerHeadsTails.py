import random


def isInt(n):
    try:
        int(n)
        return True
    except ValueError:
        return False


def inRange(beginning, end, given):
    if given >= beginning and given <= end:
        return True
    else:
        return False


def ht():
    answer = random.choice([True, False])
    choice = raw_input("Heads, or tails?").lower()
    if (choice == "h" or choice == "heads" or choice == "head") and answer:
        print "You won!"
        main()
    elif (choice == "t" or choice == "tails" or choice == "tail") and not answer:
        print "You won!"
        main()
    else:
        print "You lost!"
        main()


def hl():
    answer = random.randint(1, 100)
    print "I'm thinking of a random number between 0 and 100."
    choice = raw_input("Your guess? ")
    if isInt(raw_input()) and inRange(1, 100, int(raw_input())):



def main():
    print "Heads/tails or higher/lower?"
    option = raw_input("HT or HL? ").lower()

    if option == "ht":
        print "HT picked"
        ht()
    elif option == "hl":
        print "HL picked"
        hl()
    else:
        print "Invalid option"
        main()


main()
