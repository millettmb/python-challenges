import random
import sys

def generateNames(n):
    firstNamesFile = open("docs/firstNameList.csv", "r")
    firstNamesList = firstNamesFile.read().split('\r')
    lastNamesFile = open("docs/lastNameList.csv", "r")
    lastNamesList = lastNamesFile.read().split('\r')
    countryFile = open("docs/countryList.csv", "r")
    countryList = countryFile.read().split('\n')

    while n > 0:
        print(random.choice(firstNamesList) + " " + random.choice(lastNamesList) + " from " + random.choice(countryList))
        n -= 1


def isInt(n):
    try:
        int(n)
        return True
    except ValueError:
        return False


if len(sys.argv) > 1 and isInt(sys.argv[1]):
    generateNames(int(sys.argv[1]))
else:
    generateNames(1)
